
<div class="col-xxl-4 col-xl-4 col-lg-4  col-md-12  col-sm-12 col-12 mt-5">
				<div class="articleBd">
					<div class="artImg "><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></a></div>
					<div class="artBody px-xxl-3 px-xl-3 px-lg-3 px-md-5 px-sm-5 px-5 pb-3 pt-2">
					  
						<h4 class="fw-bold"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<?php the_excerpt();?>
						<div class="details"><?php the_category( ' ' ); ?> <?php the_date(); ?></div>
					</div>
				
				</div>
			
			</div>